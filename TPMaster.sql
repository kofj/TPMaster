/*
 Navicat MySQL Data Transfer

 Source Server         : localhost
 Source Server Version : 50533
 Source Host           : localhost
 Source Database       : dwztp321

 Target Server Version : 50533
 File Encoding         : utf-8

 Date: 05/19/2014 22:58:57 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `tpm_access`
-- ----------------------------
DROP TABLE IF EXISTS `tpm_access`;
CREATE TABLE `tpm_access` (
  `role_id` smallint(6) unsigned NOT NULL,
  `node_id` smallint(6) unsigned NOT NULL,
  `level` tinyint(1) NOT NULL,
  `pid` smallint(6) NOT NULL,
  `module` varchar(50) DEFAULT NULL,
  KEY `groupId` (`role_id`),
  KEY `nodeId` (`node_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tpm_access`
-- ----------------------------
BEGIN;
INSERT INTO `tpm_access` VALUES ('2', '1', '1', '0', null), ('2', '40', '2', '1', null), ('2', '30', '2', '1', null), ('3', '1', '1', '0', null), ('2', '69', '2', '1', null), ('2', '50', '3', '40', null), ('3', '50', '3', '40', null), ('1', '50', '3', '40', null), ('3', '7', '2', '1', null), ('3', '39', '3', '30', null), ('2', '39', '3', '30', null), ('2', '49', '3', '30', null), ('4', '1', '1', '0', null), ('4', '2', '2', '1', null), ('4', '3', '2', '1', null), ('4', '4', '2', '1', null), ('4', '5', '2', '1', null), ('4', '6', '2', '1', null), ('4', '7', '2', '1', null), ('4', '11', '2', '1', null), ('5', '25', '1', '0', null), ('5', '51', '2', '25', null), ('1', '1', '1', '0', null), ('1', '39', '3', '30', null), ('1', '69', '2', '1', null), ('1', '30', '2', '1', null), ('1', '40', '2', '1', null), ('1', '49', '3', '30', null), ('3', '69', '2', '1', null), ('3', '30', '2', '1', null), ('3', '40', '2', '1', null), ('1', '37', '3', '30', null), ('1', '36', '3', '30', null), ('1', '35', '3', '30', null), ('1', '34', '3', '30', null), ('1', '33', '3', '30', null), ('1', '32', '3', '30', null), ('1', '31', '3', '30', null), ('2', '32', '3', '30', null), ('2', '31', '3', '30', null), ('7', '90', '2', '1', null), ('7', '212', '3', '84', null), ('7', '211', '3', '84', null), ('7', '210', '3', '84', null), ('7', '209', '3', '84', null), ('7', '84', '2', '1', null), ('7', '1', '1', '0', null), ('7', '204', '3', '90', null), ('7', '207', '3', '90', null), ('7', '206', '3', '90', null), ('7', '205', '3', '90', null), ('7', '2', '2', '1', null), ('7', '219', '3', '2', null), ('7', '220', '3', '2', null), ('7', '221', '3', '2', null), ('7', '222', '3', '2', null), ('7', '6', '2', '1', null), ('7', '7', '2', '1', null), ('7', '213', '3', '7', null), ('7', '214', '3', '7', null), ('7', '215', '3', '7', null), ('7', '216', '3', '7', null), ('7', '217', '3', '7', null), ('7', '30', '2', '1', null), ('7', '31', '3', '30', null), ('7', '32', '3', '30', null), ('7', '33', '3', '30', null), ('7', '39', '3', '30', null), ('7', '37', '3', '30', null), ('7', '36', '3', '30', null), ('7', '34', '3', '30', null), ('7', '35', '3', '30', null), ('7', '49', '3', '30', null), ('7', '152', '3', '30', null), ('7', '40', '2', '1', null), ('7', '50', '3', '40', null), ('7', '125', '3', '40', null), ('7', '127', '2', '1', null), ('7', '161', '2', '1', null), ('7', '163', '2', '1', null), ('7', '168', '2', '1', null), ('7', '208', '3', '168', null);
COMMIT;

-- ----------------------------
--  Table structure for `tpm_article`
-- ----------------------------
DROP TABLE IF EXISTS `tpm_article`;
CREATE TABLE `tpm_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `pic` varchar(255) DEFAULT NULL,
  `content` text,
  `editor_id` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `createtime` char(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tpm_article`
-- ----------------------------
BEGIN;
INSERT INTO `tpm_article` VALUES ('7', '3', '咨询、规划和实施', null, '<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"524\" style=\"font-family: STHeiti;\"><tbody><tr><td align=\"right\" class=\"font-12-h2\" colspan=\"2\" height=\"84\" valign=\"top\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; line-height: 20px;\">&nbsp;</td></tr><tr><td class=\"font-12-h2\" width=\"315\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; line-height: 20px;\"><span class=\"font-12-h3\" style=\"font-size: 13px; color: rgb(140, 140, 140); line-height: 18px;\">SOREHA 咨询服务包括:</span><br /><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"font-12-h2  \" width=\"100%\" style=\"font-size: 12px; color: rgb(0, 0, 0); line-height: 20px;\"><tbody><tr><td align=\"left\" valign=\"top\" width=\"4%\"></td><td width=\"96%\"><span class=\"STYLE1\">经济分析咨询-专业化的商业计划</span></td></tr><tr><td align=\"left\" valign=\"top\">&nbsp;</td><td><span class=\"STYLE1\">康复中心空间设计-空间需求</span></td></tr><tr><td align=\"left\" valign=\"top\">&nbsp;</td><td><p class=\"STYLE1\">康复中心的组织与管理</p></td></tr><tr><td align=\"left\" valign=\"top\">&nbsp;</td><td><span class=\"STYLE1\">人力资源管理</span></td></tr><tr><td align=\"left\" valign=\"top\">&nbsp;</td><td><span class=\"STYLE1\">临床康复技术</span></td></tr><tr><td align=\"left\" colspan=\"2\" height=\"39\" valign=\"top\"><br /><span class=\"font-12-h3 STYLE1\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; color: rgb(140, 140, 140); line-height: 18px;\">康复概念:</span></td></tr><tr><td align=\"left\" valign=\"top\">&nbsp;</td><td><p class=\"STYLE1\">根据国际卫生组织ICF制定的循证医学临床康复治疗标准</p></td></tr><tr><td align=\"left\" valign=\"top\">&nbsp;</td><td>&nbsp;</td></tr></tbody></table></td><td class=\"font-12-h2\" valign=\"top\" width=\"209\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; line-height: 20px;\">&nbsp;</td></tr></tbody></table>', '5195f455477f3', '100', '0', '1368782032'), ('14', null, 'sddfas', null, 'dfassdfasdf', null, '100', '1', ''), ('15', '2', 'asdf', null, 'asdfasdfassdf', '51fb742dbf66a', '100', '0', '1375433795'), ('16', null, 'asdf', null, 'asdfadsf', null, '100', '0', ''), ('17', null, '爱上地方撒地方', null, '撒地方撒地方鞍山市地方', null, '100', '0', ''), ('18', null, '发方法', null, '反反复复', null, '100', '0', ''), ('19', null, '爱上地方爱上地方爱上地方爱上地方', null, '是打算地方爱上地方爱上地方', null, '100', '0', ''), ('20', null, '阿斯顿发生地方', null, '爱上地方', null, '100', '0', ''), ('21', null, '撒的发生地方', null, '阿斯顿发生的发生地方鞍山市地方爱上地方', null, '100', '1', ''), ('22', null, '爱上地方', null, '阿斯顿发生地方', null, '100', '0', ''), ('23', null, '爱上地方', null, '爱上地方爱上地方爱上地方爱上地方爱上地方阿斯顿发是地方爱上地方', null, '100', '0', ''), ('24', null, '阿斯顿发放的', null, '阿斯顿发的方式的士大夫爱上地方啊第三方啊的发生的发生是的发的发生的方法啊啊鞍山市地方', null, '100', '0', ''), ('25', null, 'vvvvv', null, 'vvvv', null, '100', '0', ''), ('26', null, '笑嘻嘻', null, '谢谢详细信息', null, '100', '0', ''), ('27', null, '是的分公司的风格', null, '是的风格是的风格是否大概是的风格是的分公司的分公司的是的的分公司的的分公司的分公司的的分公司的是的风格是的风格', null, '100', '0', ''), ('8', '3', '培训和教育', null, '<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: STHeiti;\"><tbody><tr><td width=\"81%\" align=\"right\"><div class=\"conbody\" style=\"margin-top: 25px; width: 550px; overflow: hidden; margin-left: 20px;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"524\"><tbody><tr><td align=\"left\" class=\"font-12-h2\" height=\"162\" valign=\"top\" width=\"315\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; line-height: 20px;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"98%\"><tbody><tr><td class=\"font-12-h2\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; line-height: 20px;\">与国际专家、临床医生以及大学合作，我们将提供综合全面的培训教育课程，教授应用最先进循证医学治疗方案<img src=\"http://ordtest-attach.stor.sinaapp.com/day_130520/201305201350337794.jpg\" alt=\"\" /></td></tr></tbody></table></td><td class=\"font-12-h2\" valign=\"top\" width=\"209\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; line-height: 20px;\">&nbsp;</td></tr><tr><td class=\"font-12-h2\" colspan=\"2\" valign=\"top\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; line-height: 20px;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"><tbody><tr class=\"tr-shixian\"><td align=\"left\" class=\"td-shixian STYLE3\" valign=\"top\" width=\"32%\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(196, 153, 73); font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: rgb(196, 153, 73); font-weight: bold;\">A级培训课程</td><td align=\"left\" class=\"td-shixian STYLE3\" valign=\"top\" width=\"34%\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(196, 153, 73); font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: rgb(196, 153, 73); font-weight: bold;\">B级培训课程</td><td align=\"left\" class=\"td-shixian STYLE3\" valign=\"top\" width=\"34%\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(196, 153, 73); font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: rgb(196, 153, 73); font-weight: bold;\">C级培训课程</td></tr><tr><td align=\"left\" class=\"td-shixian2 STYLE3\" height=\"80\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(196, 153, 73); font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px;\"><p>主动康复疗法简介</p></td><td align=\"left\" class=\"td-shixian2\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(196, 153, 73); font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px;\"><p class=\"STYLE3\">主动康复疗法-基础课程<br />• 医学训练疗法<br />• 主动康复疗法中的理疗<br />• 康复医师培训<br />&nbsp;</p></td><td align=\"left\" class=\"td-shixian2 STYLE3\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(196, 153, 73); font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px;\"><p>主动功能康复疗法（认证课程）</p></td></tr><tr><td align=\"left\" class=\"td-shixian2 STYLE3\" height=\"32\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(196, 153, 73); font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px;\">康复中心的组织和管理</td><td align=\"left\" class=\"td-shixian2 STYLE3\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(196, 153, 73); font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px;\">康复技术和设备的临床应用</td><td align=\"left\" class=\"td-shixian2 STYLE3\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(196, 153, 73); font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px;\">质量管理（认证课程）</td></tr><tr><td align=\"left\" class=\"td-shixian2 STYLE3\" height=\"30\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(196, 153, 73); font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px;\">产品操作培训</td><td align=\"left\" class=\"td-shixian2 STYLE3\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(196, 153, 73); font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px;\">海外培训课程</td><td align=\"left\" class=\"td-shixian2 STYLE3\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(196, 153, 73); font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px;\">&nbsp;</td></tr></tbody></table></td></tr></tbody></table></div></td><td width=\"11%\">&nbsp;</td></tr></tbody></table>', '5195f715783a5', '100', '1', '1368782640'), ('9', '3', '康复模块', null, '<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"528\" style=\"font-family: STHeiti;\"><tbody><tr><td align=\"left\" class=\"font-12-h2\" rowspan=\"2\" valign=\"top\" width=\"227\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; line-height: 20px;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"96%\"><tbody><tr><td class=\"font-12-h2\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; line-height: 20px;\">SOREHA提供针对不同适应症和不同病人群体需求的康复治疗模块<br /><br /><p>客户将得到：</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"font-12-h2 \" width=\"100%\" style=\"font-size: 12px; color: rgb(0, 0, 0); line-height: 20px;\"><tbody><tr><td align=\"left\" valign=\"top\" width=\"6%\"></td><td width=\"93%\"><p class=\"STYLE3\">商业计划和市场宣传方案</p></td></tr><tr><td align=\"left\" valign=\"top\">&nbsp;</td><td><p class=\"STYLE3\">员工教育培训计划</p></td></tr><tr><td align=\"left\" valign=\"top\">&nbsp;</td><td><p class=\"STYLE3\">质量管理课程（认证课程）</p></td></tr></tbody></table></td></tr></tbody></table></td><td align=\"middle\" class=\"font-12-h3\" height=\"31\" valign=\"top\" width=\"301\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; color: rgb(140, 140, 140); line-height: 18px;\">SOREHA 康复治疗模块</td></tr><tr><td class=\"font-12-h2\" valign=\"top\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; line-height: 20px;\">&nbsp;</td></tr></tbody></table>', '5195f73ae8d99', '100', '1', '1368782671'), ('10', '3', '售后服务', null, '<table width=\"1000\" height=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: STHeiti;\"><tbody><tr><td width=\"660\" valign=\"top\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tbody><tr><td width=\"81%\" align=\"right\"><div class=\"conbody\" style=\"margin-top: 25px; width: 550px; overflow: hidden; margin-left: 20px;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"525\"><tbody><tr><td class=\"font-12-h2\" valign=\"top\" width=\"320\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; line-height: 20px;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"99%\"><tbody><tr><td class=\"font-12-h2\" valign=\"top\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; line-height: 20px;\"><p>SOREHA 承诺良好的售后服务<br /><br />我们的目标是通过良好的产品质量和全方位的售后服务与客户建立长期合作关系。我们售后服务包括设备的软硬件的技术维护和临床技术的培训和临床技术支持<br /><br />专业人员会通过热线电话解答您所有的问题。技术支持小组会在最短的时间里解决您的技术问题<br /><br />我们还将提供服务和质量保证合同</p></td></tr></tbody></table></td><td align=\"middle\" class=\"font-12-h3\" valign=\"top\" width=\"205\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; color: rgb(140, 140, 140); line-height: 18px;\">&nbsp;</td></tr></tbody></table></div></td><td width=\"11%\">&nbsp;</td></tr></tbody></table></td></tr><tr><td height=\"7%\" colspan=\"2\" valign=\"bottom\">&nbsp;</td></tr></tbody></table>', '5195f7591bb7d', '100', '1', '1368782723'), ('11', '2', '为用户提供完整解决方案', null, '<p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">除了提供世界一流的康复设备以外，我公司也为我们的客户提供康复机构的规划、设计咨询。总体而言，我们会从以下多个方面搜集用户信息，从而为用户提供更进一步的服务：</span></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">&nbsp;</span></p><p><strong><span style=\"font-family:Microsoft YaHei;font-size:16px;\">康复机构的性质：</span></strong></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">□康复专科医院 □综合医院康复科 □门诊康复中心</span></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">&nbsp;</span></p><p><strong><span style=\"font-family:Microsoft YaHei;font-size:16px;\">康复机构的适应症：</span></strong></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">&nbsp;</span></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">□骨科 </span></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">□创伤 □脊柱外科 □关节外科 □关节镜 □颈肩腰腿痛的保守治疗</span></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">□神经科 </span></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">□儿童康复（小儿脑瘫）</span></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">□运动员康复</span></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">□老年康复</span></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">□残疾人康复</span></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">□工伤康复（工作模拟、工作强化）</span></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">&nbsp;</span></p><p><strong><span style=\"font-family:Microsoft YaHei;font-size:16px;\">如果是康复专科医院或者综合医院康复科</span></strong></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">康复病床数&nbsp; □50以下 □50-100 □100-150 □150-200 □200以上</span></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">骨科病床数&nbsp; □50以下 □50-100 □100-150 □150-200 □200以上</span></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">神经科病床数□50以下 □50-100 □100-150 □150-200 □200以上</span></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">&nbsp;</span></p><p><strong><span style=\"font-family:Microsoft YaHei;font-size:16px;\">预计每天多少病人在康复中心介绍治疗</span></strong></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">□50人以下 □50-100人 □100-150人 □150-200人 □200人以上</span></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">&nbsp;</span></p><p><strong><span style=\"font-family:Microsoft YaHei;font-size:16px;\">康复机构的运营面积及建筑施工图</span></strong></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">&nbsp;</span></p><p><strong><span style=\"font-family:Microsoft YaHei;font-size:16px;\">大致的预算 （人民币 万元）</span></strong></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">□100以下 □100-300 □300-500 □500-1000 □1000以上</span></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">&nbsp;</span></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">我们将根据用户提供的以上信息，依据中国卫生部对相关康复医疗机构的建设要求和欧洲康复机构的质量管理标准，为用户提供一个以主动康复内容为主的康复机构建设解决方案：</span></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">包括：</span></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">a)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 康复中心功能区的规划</span></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">b)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 康复设备的配置表</span></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">c)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 设备摆放示意图</span></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">d)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 康复中心人员配置</span></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">e)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1 - 3年临床教育培训和临床技术支持计划</span></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">&nbsp;</span></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">（下面是一个综合医院康复中心的功能区平面图和设备摆放示意图。然后我对这个示意图所代表的康复中心做一个简单描述。）</span></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">&nbsp;</span></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">&nbsp;</span></p>', '5195f7dd6f5cd', '100', '1', '1368782857'), ('12', '1', '核心竞争力', null, '<p align=\"center\">&nbsp;</p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 欧培德公司之所以能够在中国康复医疗市场独树一帜，持续发展是因为具有如下竞争优势：</span></p><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">&nbsp;</span></p><ul><li><strong><span style=\"font-family:Microsoft YaHei;font-size:16px;\">深刻理解现代医学康复发展潮流，完整掌握主动康复理论思想和临床应用方法</span></strong></li></ul><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 我公司经过多年对康复医学发展的跟踪研究和市场调研，确定了推广主动康复的明确指导方向。公司的核心成员大多接受过西方主动康复教育，都是主动康复坚定的支持者。他们根据多年在欧洲从事主动康复的临床经验，建立了一整套主动临床应用的教学方法</span></p><ul><li><strong><span style=\"font-family:Microsoft YaHei;font-size:16px;\">与欧洲开展主动康复的著名医疗机构建立广泛深入的合作，为主动康复在中国的推广提供了坚实的物质基础</span></strong></li></ul><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 我公司先后与德国科隆大学附属医院康复中心、德国勒沃库森Rehatrain 康复中心、德国orthotrain 康复中心和瑞士莱茵费尔登康复医院签订了临床培训合作协议。近年来，公司安排多批国内康复机构的医生和治疗师到这些合作机构中进修、学习。</span></p><ul><li><span style=\"font-size:16px;\"><span style=\"font-family:Microsoft YaHei;\">&nbsp;<strong>德国临床康复专家团队系统推广主动康复</strong></span></span></li></ul><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 我公司历来把临床康复的培训教育视为公司业务发展的核心内容。为此公司在创办初期就开始着手临床康复专家团队的建设。经过多年努力，目前以公司CEO Oliver Kieffer博士为首的德国康复专家团队已经建立，他们为公司几十个客户单位的数百名康复医生和治疗师提供了系统的临床康复教育培训。因为有了这只专家团队，公司与客户1-3年的临床教育培训项目才得以顺利实施。</span></p><ul><li><strong><span style=\"font-family:Microsoft YaHei;font-size:16px;\">与世界国际最知名品牌的主动康复技术和设备厂家合作</span></strong></li></ul><p><span style=\"font-family:Microsoft YaHei;font-size:16px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 由于与公司推广主动康复的发展方向和重视临床康复教育培训的指导思想高度一致，许多国际一流品牌的康复技术设备提供商与我公司结成市场战略同盟。MTT（医学训练疗法）的先驱德国Proxomed公司，工作模拟和工作强化测试评估设备的世界领导企业美国BTE公司，世界康复机器人联盟领袖瑞士Hocoma公司等多家国际知名企业先后与公司合作共同开拓中国康复市场。</span></p>', '5195f829c4a26', '100', '1', '1368782930'), ('13', '1', '神经康复训练', null, '<span style=\"color: rgb(140, 140, 140); font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px;\">步态训练和神经肌肉再训练</span>', '5195f85fa596e', '100', '1', '1368782956'), ('29', null, '公仔一个', null, 'gasdgasdg', null, '100', '0', ''), ('30', null, 'ddddd', null, 'dddddddd', null, '100', '0', ''), ('31', null, '再测试一下吧', null, '再测试一下吧', null, '100', '0', ''), ('32', null, 'ddddd2324', null, 'dddddd', null, '100', '1', ''), ('34', null, '你想说啥？', null, '我想说我想你了。', null, '100', '0', '');
COMMIT;

-- ----------------------------
--  Table structure for `tpm_form`
-- ----------------------------
DROP TABLE IF EXISTS `tpm_form`;
CREATE TABLE `tpm_form` (
  `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `create_time` int(11) unsigned NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `tpm_group`
-- ----------------------------
DROP TABLE IF EXISTS `tpm_group`;
CREATE TABLE `tpm_group` (
  `id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `title` varchar(50) NOT NULL,
  `create_time` int(11) unsigned NOT NULL,
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sort` smallint(3) unsigned NOT NULL DEFAULT '0',
  `show` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `group_menu` char(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tpm_group`
-- ----------------------------
BEGIN;
INSERT INTO `tpm_group` VALUES ('9', 'webapp', '网站管理', '1316267260', '0', '1', '2', '0', 'About'), ('2', 'System', '系统设置', '1222841259', '0', '1', '20', '0', 'info'), ('24', 'News', '信息管理', '1375687191', '0', '1', '0', '0', 'News');
COMMIT;

-- ----------------------------
--  Table structure for `tpm_group_class`
-- ----------------------------
DROP TABLE IF EXISTS `tpm_group_class`;
CREATE TABLE `tpm_group_class` (
  `id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `menu` char(10) NOT NULL DEFAULT '0',
  `name` char(25) NOT NULL,
  `status` int(1) unsigned NOT NULL DEFAULT '0',
  `sort` int(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tpm_group_class`
-- ----------------------------
BEGIN;
INSERT INTO `tpm_group_class` VALUES ('1', 'info', '系统管理', '1', '100'), ('21', 'About', '网站管理', '1', '100');
COMMIT;

-- ----------------------------
--  Table structure for `tpm_group_class_user`
-- ----------------------------
DROP TABLE IF EXISTS `tpm_group_class_user`;
CREATE TABLE `tpm_group_class_user` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `gc_id` int(6) DEFAULT NULL,
  `uid` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `gc` (`gc_id`,`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tpm_group_class_user`
-- ----------------------------
BEGIN;
INSERT INTO `tpm_group_class_user` VALUES ('28', '3', '2'), ('29', '3', '3'), ('30', '3', '4'), ('7', '1', '1'), ('8', '1', '2'), ('9', '1', '3'), ('10', '1', '4'), ('11', '1', '38'), ('12', '1', '39'), ('13', '2', '1'), ('15', '2', '3'), ('16', '2', '4'), ('17', '2', '38'), ('18', '2', '39'), ('19', '5', '1'), ('20', '5', '2'), ('21', '5', '3'), ('22', '5', '4'), ('23', '5', '38'), ('24', '5', '39'), ('31', '3', '38');
COMMIT;

-- ----------------------------
--  Table structure for `tpm_log`
-- ----------------------------
DROP TABLE IF EXISTS `tpm_log`;
CREATE TABLE `tpm_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `vc_module` char(50) DEFAULT NULL,
  `vc_operation` char(200) DEFAULT NULL,
  `creator_id` bigint(20) DEFAULT NULL,
  `creator_name` char(50) NOT NULL,
  `vc_ip` char(50) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `createtime` char(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=349 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tpm_log`
-- ----------------------------
BEGIN;
INSERT INTO `tpm_log` VALUES ('193', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1357458137'), ('194', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1357465574'), ('195', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1357465641'), ('196', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1357466884'), ('197', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1357467126'), ('198', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1357651253'), ('199', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1357782822'), ('200', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1357797558'), ('201', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1357800986'), ('202', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359093817'), ('203', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359094193'), ('204', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359094345'), ('205', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359094584'), ('206', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359095428'), ('207', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359095848'), ('208', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359096462'), ('209', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359096633'), ('210', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359098124'), ('211', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359100841'), ('212', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359101561'), ('213', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359103097'), ('214', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359105228'), ('215', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359106258'), ('216', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359121924'), ('217', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359125362'), ('218', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359125571'), ('219', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359125627'), ('220', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359185573'), ('221', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359267144'), ('222', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359267684'), ('223', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359272715'), ('224', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359272873'), ('225', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359273452'), ('226', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359273592'), ('227', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359276852'), ('228', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359280386'), ('229', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359290308'), ('230', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359337085'), ('231', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359340361'), ('232', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359422581'), ('233', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359526603'), ('234', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359681824'), ('235', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359971788'), ('236', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1359986370'), ('237', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1360027242'), ('238', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1360030220'), ('239', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1360034854'), ('240', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1360044287'), ('241', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1360049122'), ('242', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1360049790'), ('243', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1360072973'), ('244', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1360118308'), ('245', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1360123971'), ('246', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1360901668'), ('247', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1361282013'), ('248', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1361419199'), ('249', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1361425888'), ('250', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1361523138'), ('251', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1361523296'), ('252', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1361844689'), ('253', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1361952062'), ('254', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1362102219'), ('255', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1362473189'), ('256', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1362535063'), ('257', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1362536424'), ('258', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1362707577'), ('259', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1362713668'), ('260', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1362972108'), ('261', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1363226347'), ('262', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1363570392'), ('263', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1363855305'), ('264', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1363857327'), ('265', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1363857406'), ('266', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1363873286'), ('267', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1363919952'), ('268', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1363922817'), ('269', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1363922932'), ('270', '系统管理', '用户登录：登录成功！', '2', 'demo', '127.0.0.1', '1', '1363923072'), ('271', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1363923348'), ('272', '系统管理', '用户登录：登录成功！', '2', 'demo', '127.0.0.1', '1', '1363923450'), ('273', '系统管理', '用户登录：登录成功！', '2', 'demo', '127.0.0.1', '1', '1363923613'), ('274', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1363923669'), ('275', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1364372564'), ('276', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1367918555'), ('277', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1367919475'), ('278', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1367930908'), ('279', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1367933590'), ('280', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1367934584'), ('281', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1367934697'), ('282', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1367978752'), ('283', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1367979176'), ('284', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1368008929'), ('285', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1368085596'), ('286', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1368149159'), ('287', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1368152124'), ('288', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1368159516'), ('289', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1368163146'), ('290', '系统管理', '用户登录：登录成功！', '1', 'admin', '218.249.123.122', '1', '1368677212'), ('291', '系统管理', '用户登录：登录成功！', '1', 'admin', '219.143.150.255', '1', '1368682645'), ('292', '系统管理', '用户登录：登录成功！', '1', 'admin', '123.116.237.235, 123.116.237.235', '1', '1368696109'), ('293', '系统管理', '用户登录：登录成功！', '1', 'admin', '1.202.5.53', '1', '1368778169'), ('294', '系统管理', '用户登录：登录成功！', '1', 'admin', '123.116.237.235, 123.116.237.235', '1', '1368782595'), ('295', '系统管理', '用户登录：登录成功！', '1', 'admin', '219.143.151.68', '1', '1369028980'), ('296', '系统管理', '用户登录：登录成功！', '1', 'admin', '124.126.222.134', '1', '1369293589'), ('297', '系统管理', '用户登录：登录成功！', '1', 'admin', '124.126.222.134', '1', '1369294148'), ('298', '系统管理', '用户登录：登录成功！', '1', 'admin', '123.116.244.81, 123.116.244.81', '1', '1369647246'), ('299', '系统管理', '用户登录：登录成功！', '1', 'admin', '123.116.244.81, 123.116.244.81', '1', '1369647473'), ('300', '系统管理', '用户登录：登录成功！', '1', 'admin', '123.116.244.81, 123.116.244.81', '1', '1369726229'), ('301', '系统管理', '用户登录：登录成功！', '1', 'admin', '123.116.244.81, 123.116.244.81', '1', '1369734806'), ('302', '系统管理', '用户登录：登录成功！', '1', 'admin', '1.202.7.211', '1', '1369735092'), ('303', '系统管理', '用户登录：登录成功！', '1', 'admin', '1.202.7.211', '1', '1369736194'), ('304', '系统管理', '用户登录：登录成功！', '1', 'admin', '1.202.7.211', '1', '1369736543'), ('305', '系统管理', '用户登录：登录成功！', '1', 'admin', '123.122.94.188, 123.122.94.188', '1', '1369748666'), ('306', '系统管理', '用户登录：登录成功！', '1', 'admin', '123.116.244.81, 123.116.244.81', '1', '1369797917'), ('307', '系统管理', '用户登录：登录成功！', '1', 'admin', '1.202.7.211', '1', '1369815797'), ('308', '系统管理', '用户登录：登录成功！', '1', 'admin', '123.116.234.192, 123.116.234.192', '1', '1369816898'), ('309', '系统管理', '用户登录：登录成功！', '1', 'admin', '123.116.234.192, 123.116.234.192', '1', '1369822196'), ('310', '系统管理', '用户登录：登录成功！', '1', 'admin', '1.202.7.211', '1', '1369822239'), ('311', '系统管理', '用户登录：登录成功！', '1', 'admin', '1.202.7.211', '1', '1369823923'), ('312', '系统管理', '用户登录：登录成功！', '1', 'admin', '123.116.234.192, 123.116.234.192', '1', '1369824096'), ('313', '系统管理', '用户登录：登录成功！', '1', 'admin', '219.142.232.230', '1', '1369881820'), ('314', '系统管理', '用户登录：登录成功！', '1', 'admin', '219.142.232.230', '1', '1369882594'), ('315', '系统管理', '用户登录：登录成功！', '1', 'admin', '219.142.232.230', '1', '1369882839'), ('316', '系统管理', '用户登录：登录成功！', '1', 'admin', '219.142.232.230', '1', '1369891797'), ('317', '系统管理', '用户登录：登录成功！', '1', 'admin', '219.142.232.230', '1', '1369892498'), ('318', '系统管理', '用户登录：登录成功！', '1', 'admin', '123.116.234.192, 123.116.234.192', '1', '1369897214'), ('319', '系统管理', '用户登录：登录成功！', '1', 'admin', '123.116.234.192, 123.116.234.192', '1', '1369905994'), ('320', '系统管理', '用户登录：登录成功！', '1', 'admin', '123.116.229.89, 123.116.229.89', '1', '1369916770'), ('321', '系统管理', '用户登录：登录成功！', '1', 'admin', '114.244.5.58, 114.244.5.58', '1', '1369926815'), ('322', '系统管理', '用户登录：登录成功！', '1', 'admin', '123.116.234.192, 123.116.234.192', '1', '1369963985'), ('323', '系统管理', '用户登录：登录成功！', '1', 'admin', '219.143.150.115', '1', '1369966141'), ('324', '系统管理', '用户登录：登录成功！', '1', 'admin', '123.116.234.192, 123.116.234.192', '1', '1369979193'), ('325', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1374580271'), ('326', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1374634907'), ('327', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1374807183'), ('328', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1374807183'), ('329', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1374808641'), ('330', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1374808641'), ('331', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1374810400'), ('332', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1374810611'), ('333', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1374811182'), ('334', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1374811182'), ('335', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1374811469'), ('336', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1374812108'), ('337', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1374833449'), ('338', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1375068511'), ('339', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1375172123'), ('340', '系统管理', '用户登录：登录成功！', '2', 'demo', '127.0.0.1', '1', '1375172148'), ('341', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1375172161'), ('342', '系统管理', '用户登录：登录成功！', '2', 'demo', '127.0.0.1', '1', '1375172199'), ('343', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1375172208'), ('344', '系统管理', '用户登录：登录成功！', '2', 'demo', '127.0.0.1', '1', '1375172281'), ('345', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1375172297'), ('346', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1375327649'), ('347', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1375350540'), ('348', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1375433752');
COMMIT;

-- ----------------------------
--  Table structure for `tpm_node`
-- ----------------------------
DROP TABLE IF EXISTS `tpm_node`;
CREATE TABLE `tpm_node` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `remark` varchar(255) DEFAULT NULL,
  `sort` smallint(6) unsigned DEFAULT NULL,
  `pid` smallint(6) unsigned NOT NULL,
  `level` tinyint(1) unsigned NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `group_id` tinyint(3) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `level` (`level`),
  KEY `pid` (`pid`),
  KEY `status` (`status`),
  KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=223 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tpm_node`
-- ----------------------------
BEGIN;
INSERT INTO `tpm_node` VALUES ('50', 'main', '空白首页', '1', '', null, '40', '3', '0', '0'), ('84', 'Group', '分组管理', '1', '', '1', '1', '2', '0', '2'), ('90', 'Article', '文章管理', '1', '', '100', '1', '2', '0', '9'), ('93', 'index', '首页管理', '1', '', '0', '92', '3', '0', '0'), ('96', 'index', '列表页', '1', '', '0', '95', '3', '0', '0'), ('1', 'Admin', '后台管理', '1', '', '0', '0', '1', '0', '0'), ('2', 'Node', '节点管理', '1', '', '2', '1', '2', '0', '2'), ('6', 'Role', '角色管理', '1', '', '1', '1', '2', '0', '2'), ('7', 'User', '后台用户', '1', '', '3', '1', '2', '0', '2'), ('30', 'Public', '公共模块', '1', '', '5', '1', '2', '0', '0'), ('31', 'add', '新增', '1', '', null, '30', '3', '0', '0'), ('32', 'insert', '写入', '1', '', null, '30', '3', '0', '0'), ('33', 'edit', '编辑', '1', '', null, '30', '3', '0', '0'), ('39', 'index', '列表', '1', '', null, '30', '3', '0', '0'), ('37', 'resume', '恢复', '1', '', null, '30', '3', '0', '0'), ('36', 'forbid', '禁用', '1', '', null, '30', '3', '0', '0'), ('34', 'update', '更新', '1', '', null, '30', '3', '0', '0'), ('35', 'foreverdelete', '删除', '1', '', null, '30', '3', '0', '0'), ('40', 'Index', '默认模块', '1', '', '1', '1', '2', '0', '0'), ('49', 'read', '查看', '1', '', null, '30', '3', '0', '0'), ('125', 'index', '首页模块', '1', null, null, '40', '3', '0', '0'), ('127', 'Log', '登录日志管理', '1', '', '8', '1', '2', '0', '2'), ('133', 'index', '首页', '1', '', '0', '132', '3', '0', '0'), ('152', 'detail', '详情', '1', '', '0', '30', '3', '0', '0'), ('161', 'Bak', '数据库备份', '1', '', '6', '1', '2', '0', '2'), ('163', 'GroupClass', '系统导航管理', '1', '', '0', '1', '2', '0', '2'), ('168', 'File', '程序文件管理', '0', '', '10', '1', '2', '0', '2'), ('204', 'index', '列表', '1', '', '100', '90', '3', '0', '0'), ('208', 'index', '列表', '1', '', '100', '168', '3', '0', '0'), ('209', 'add', '新增', '1', '', '100', '84', '3', '0', '0'), ('210', 'edit', '修改', '1', '', '100', '84', '3', '0', '0'), ('211', 'index', '列表', '1', '', '100', '84', '3', '0', '0'), ('207', 'foreverdelete', '删除', '1', '', '100', '90', '3', '0', '0'), ('206', 'edit', '修改', '1', '', '100', '90', '3', '0', '0'), ('205', 'add', '新增', '1', '', '100', '90', '3', '0', '0'), ('212', 'foreverdelete', '删除', '1', '', '100', '84', '3', '0', '0'), ('213', 'foreverdelete', '删除', '1', '', '100', '7', '3', '0', '0'), ('203', 'add', '添加', '1', '', '100', '202', '3', '0', '0'), ('214', 'add', '新增', '1', '', '100', '7', '3', '0', '0'), ('215', 'edit', '修改', '1', '', '100', '7', '3', '0', '0'), ('216', 'index', '列表', '1', '', '100', '7', '3', '0', '0'), ('217', 'password', '修改密码', '1', '', '100', '7', '3', '0', '0'), ('219', 'foreverdelete', '删除', '1', '', '100', '2', '3', '0', '0'), ('220', 'add', '新增', '1', '', '100', '2', '3', '0', '0'), ('221', 'edit', '修改', '1', '', '100', '2', '3', '0', '0'), ('222', 'index', '列表', '1', '', '100', '2', '3', '0', '0');
COMMIT;

-- ----------------------------
--  Table structure for `tpm_role`
-- ----------------------------
DROP TABLE IF EXISTS `tpm_role`;
CREATE TABLE `tpm_role` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `pid` smallint(6) DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `ename` varchar(5) DEFAULT NULL,
  `create_time` int(11) unsigned NOT NULL,
  `update_time` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parentId` (`pid`),
  KEY `ename` (`ename`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tpm_role`
-- ----------------------------
BEGIN;
INSERT INTO `tpm_role` VALUES ('1', '领导组', '0', '1', '', '', '1208784792', '1254325558'), ('2', '员工组', '0', '1', '', '', '1215496283', '1254325566'), ('7', '演示组', '0', '1', '', null, '1254325787', '0'), ('8', 'test123', '0', '1', 'test', null, '1400423752', '1400423869');
COMMIT;

-- ----------------------------
--  Table structure for `tpm_role_user`
-- ----------------------------
DROP TABLE IF EXISTS `tpm_role_user`;
CREATE TABLE `tpm_role_user` (
  `role_id` mediumint(9) unsigned DEFAULT NULL,
  `user_id` char(32) DEFAULT NULL,
  KEY `group_id` (`role_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tpm_role_user`
-- ----------------------------
BEGIN;
INSERT INTO `tpm_role_user` VALUES ('4', '27'), ('4', '26'), ('4', '30'), ('5', '31'), ('3', '22'), ('3', '1'), ('1', '4'), ('2', '3'), ('7', '2'), ('3', '35');
COMMIT;

-- ----------------------------
--  Table structure for `tpm_user`
-- ----------------------------
DROP TABLE IF EXISTS `tpm_user`;
CREATE TABLE `tpm_user` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `account` varchar(64) NOT NULL,
  `nickname` varchar(50) NOT NULL,
  `password` char(32) NOT NULL,
  `bind_account` varchar(50) NOT NULL,
  `last_login_time` int(11) unsigned DEFAULT '0',
  `last_login_ip` varchar(40) DEFAULT NULL,
  `login_count` mediumint(8) unsigned DEFAULT '0',
  `verify` varchar(32) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `create_time` int(11) unsigned NOT NULL,
  `update_time` int(11) unsigned NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `type_id` tinyint(2) unsigned DEFAULT '0',
  `info` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account` (`account`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tpm_user`
-- ----------------------------
BEGIN;
INSERT INTO `tpm_user` VALUES ('1', 'admin', '管理员', '21232f297a57a5a743894a0e4a801fc3', '', '1400507566', '127.0.0.1', '895', '8888', 'liu21st@gmail.com', '备注信息123341111呃呃呃呃', '1222907803', '1400338549', '1', '0', ''), ('2', 'demo', '演示', 'fe01ce2a7fbac8fafaed7c982a04e229', '', '1254326091', '127.0.0.1', '90', '8888', '', '', '1239783735', '1254325770', '1', '0', ''), ('3', 'member', '员工', 'aa08769cdcb26674c6706093503ff0a3', '', '1326266720', '127.0.0.1', '17', '', '', '', '1253514375', '1254325728', '1', '0', ''), ('4', 'leader', '领导', 'c444858e0aaeb727da73d2eae62321ad', '', '1254325906', '127.0.0.1', '15', '', '', '领导', '1253514575', '1254325705', '1', '0', ''), ('35', 'lxz123', '哈哈', 'e10adc3949ba59abbe56e057f20f883e', '', '0', null, '0', null, '646649009@qq.com', '', '1400424386', '1400424438', '1', '0', '');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
