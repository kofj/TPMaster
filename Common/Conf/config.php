<?php
return array(
	//'配置项'=>'配置值'
	'DEFAULT_MODULE'    => 'Hello',

    'URL_MODEL'         => 2, // 如果你的环境不支持PATHINFO 请设置为3
	//数据库配置
    'DB_TYPE'           => 'mysql',
    'DB_HOST'           => '127.0.0.1',
    'DB_NAME'           => 'tpmaster',
    'DB_USER'           => 'tpmaster',
    'DB_PWD'            => 'tpmaster',
    'DB_PORT'           => '3306',
    'DB_PREFIX'         => 'tpm_',

    'LOAD_EXT_CONFIG'   => array('ALIOSS' => 'alioss',) // 加载扩展配置

);