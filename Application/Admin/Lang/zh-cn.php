<?php
return array(
     'lan_define'=>'欢迎使用ThinkPHP',
     'Old Password'			=> '原密码',
     "New Password"			=> '新密码',
     "Confim Password"		=> '确认密码',
     "Identifying code"		=> '验证码',
     "Submit"				=> '提交',
     "Cancel"				=> '取消',
     "Nickname"				=> '昵称',
     "Email"				=> '邮箱',
     "Remark"				=> '备注',
     "System Info"			=> '系统信息',
     "User Name"			=> '用户名',
     "Password"				=> '密码',
     "Click to refresh"		=> '点击刷新验证码',
     "Report"				=> '反馈',
     "Help"					=>	'帮助',
);
