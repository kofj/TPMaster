<?php
use Org\Util\Dir;
class CacheController extends CommonController {
	public function index() {
		$this->clear();
	}

	private function clear() {
		//Dir::delDir( TEMP_PATH );
		Dir::delDir( CACHE_PATH );
		//$this->ajaxReturn($);
	}

	/* 
	 * 清除Memcached缓存
	 * 
	 */
	public function clearMem() {}


}