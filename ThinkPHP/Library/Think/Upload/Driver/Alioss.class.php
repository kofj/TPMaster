<?php
namespace Think\Upload\Driver;
use Aliyun\OSS\OSSClient;
require_once 'Alioss/aliyun.php'; //SDK

class Alioss {
	/**
     * 上传文件根目录
     * @var string
     */
    private $rootPath;

    /**
     * 上传错误信息
     * @var string
     */
    private $error = '';

    public $config = array(
    	'Endpoint'			=> '',
    	'AccessKeyId'		=> '',
        'AccessKeySecret'	=> '',
        'Bucket'			=> '', //空间名称
        'rootPath'			=> '/',
    );

    private $oss;
    
    /**
     * 构造函数，用于设置上传根路径
     */
    public function __construct($config = array()) {
    	$this->config = array_merge($this->config, C ('ALIOSS') );
    	$this->oss = OSSClient::factory( $this->config );
    }

    /**
     * 检测上传根目录(百度云上传时支持自动创建目录，直接返回)
     * @param string $rootpath   根目录
     * @return boolean true-检测通过，false-检测失败
     */
    public function checkRootPath($rootpath){
        /* 设置根目录 */
        $this->config['rootPath'] = str_replace('./', '/', $this->config['rootPath']);
    	return true;
    }

    /**
     * 检测上传目录(Alioss上传时支持自动创建目录，直接返回)
     * @param  string $savepath 上传目录
     * @return boolean          检测结果，true-通过，false-失败
     */
	public function checkSavePath($savepath){
		return true;
    }

    /**
     * 创建文件夹 (Alioss上传时支持自动创建目录，直接返回)
     * @param  string $savepath 目录名称
     * @return boolean          true-创建成功，false-创建失败
     */
    public function mkdir($savepath){
    	return true;
    }

    /**
     * 获取最后一次上传错误信息
     * @return string 错误信息
     */
    public function getError(){
        return $this->error;
    }

    /**
     * 保存指定文件到存储
     * @param  array   $file    保存的文件信息
     * @param  boolean $replace 同名文件是否覆盖
     * @return boolean          保存状态，true-成功，false-失败
     */
    public function save($file, $replace=true) {
    	try {
	    	$this -> oss -> putObject (array(
	    		'Bucket'		=> $this->config['Bucket'],
	    		'Key'			=> $file['savepath'] . $file['savename'],
	    		'Content'		=> fopen($file['tmp_name'], 'rb'),
	    		'ContentLength'	=> filesize($file['tmp_name']),
	    		/* 出现错误： SignatureDoesNotMatch
	    		'UserMetadata'	=> array(
	    			'Original Name'	=> $file['name'],
	    			'md5'			=> $file['md5'],
	    			'sha1'			=> $file['sha1'],
	    		),
	    		//*/
	    	));
    	} catch (\Aliyun\OSS\Exceptions\OSSException $ex) {
    		$this->error = "Alioss Error： " . $ex->getErrorCode() . "\n";
    	} catch (\Aliyun\Common\Exceptions\ClientException $ex) {
    		$this->error = "Alioss ClientError： " . $ex->getMessage() . "\n";
    	}
    	if ('' == $this->error) {
    		return true;
    	}else{
    		return false;
    	}
    }

    /**
     * 获取用户所有的Bucket
     */
    public function buckets() {
    	$ret = array();
    	try {
	    	$buckets = $this->oss->listBuckets();
    	} catch (\Aliyun\OSS\Exceptions\OSSException $ex) {
    		$this->error = "Alioss Error： " . $ex->getErrorCode() . "\n";
    	} catch (\Aliyun\Common\Exceptions\ClientException $ex) {
    		$this->error = "Alioss ClientError： " . $ex->getMessage() . "\n";
    	}
    	foreach ($buckets as $bucket) {
    		$ret[] = $bucket->getName();
    	}
    	return $ret;
    }

    /**
     * 新建Bucket 
     * @param string name Bucket名称
     */
    public function createBucket( $name ) {
    	try {
    		$this->oss->createBucket( array( 'Bucket' => $name,));
    	} catch (\Aliyun\OSS\Exceptions\OSSException $ex) {
    		$this->error = "Alioss Error： " . $ex->getErrorCode() . "\n";
    	} catch (\Aliyun\Common\Exceptions\ClientException $ex) {
    		$this->error = "Alioss ClientError： " . $ex->getMessage() . "\n";
    	}
    }

}