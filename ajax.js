function navTabAjaxDone(json){
        DWZ.ajaxDone(json);
        if(json.statusCode==DWZ.statusCode.ok){
            if(json.navTabId){
                //先判断当前的nav是否有 pagerForm，
                //有，就刷新这个nav 中的分页控件
                var $pageForm = $("form[name="+json.navTabId+"_pageForm]");
                if($pageForm){
                    console.log("刷新分页");
                    $pageForm.submit();
                }else{
                    console.log("刷新本Nav");
                    navTab.reloadFlag(json.navTabId);
                }
            }else{
                navTabPageBreak({},json.rel);
            }
            if("closeCurrent"==json.callbackType){
                setTimeout(function(){navTab.closeCurrentTab();},100);
            }else if("forward"==json.callbackType){
                navTab.reload(json.forwardUrl);
            }
        }
    }